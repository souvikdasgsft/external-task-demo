package com.gsft.api.marketplace.api.java.lms.config;

import java.util.List;

import org.camunda.bpm.client.backoff.BackoffStrategy;
import org.camunda.bpm.client.task.ExternalTask;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BackoffStrategyConfiguration
{
	@Value("${camunda.bpm.client.backoff-strategy-time}")
	private Long backoffStrategyTime;

	@Bean
	public BackoffStrategy backoffStrategy()
	{
		return new BackoffStrategy()
		{
			@Override
			public void reconfigure(List<ExternalTask> list)
			{
			}

			@Override
			public long calculateBackoffTime()
			{
				return backoffStrategyTime;
			}
		};
	}
}