package com.gsft.api.marketplace.api.java.lms.service.camunda.external;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

import org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskHandler;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@ExternalTaskSubscription("EXTERNAL_TOPIC")
@AllArgsConstructor
public class ExtractLoanTaskHandler implements ExternalTaskHandler
{
	@Override
	public void execute(ExternalTask externalTask, ExternalTaskService externalTaskService)
	{
		try
		{
			String name = externalTask.getVariable("name");
			log.info("name received from workflow:{}", name);
			VariableMap variables = Variables.createVariables();
			Map<String, Object> result = Map.of("name", name, "age", 23, "gender", "M");
			variables.putAll(result);
			externalTaskService.complete(externalTask, variables);
		}
		catch(Exception e)
		{
			log.error(
				"Some exception occured while executing loan external task ", e);
			externalTaskService.handleBpmnError(externalTask, "General Exception", e.getMessage());
		}
		log.info("External task completed ");
	}
}
